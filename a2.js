var words = ['Pineapple']
var chars = 'ANIMAL0123456789'
var animals = [
    // "()>_<()",
    // "Ƹ̵̡Ӝ̵̨̄Ʒ",
    "(♥_♥)",
    // "¯\\_(ツ)_/¯ ",
    // "^⨀ᴥ⨀^",
    // "^ↀᴥↀ^",
    "ヽ(￣(ｴ)￣)ﾉ",
    "ヽ(￣(ｴ)￣)ﾉ",
    "ヽ(￣(ｴ)￣)ﾉ",
    "ヽ(￣(ｴ)￣)ﾉ",
    "ヽ(￣(ｴ)￣)ﾉ",
    "ヽ(￣(ｴ)￣)ﾉ",
    // "ʕ•ᴥ•ʔ",
    // "ˁ(⦿ᴥ⦿)ˀ"
    // " ۜ\\(סּںסּَ` )/ۜ",
    // "•͡˘㇁•͡˘",
    // " (╯_╰)",
    // "~(‾▿‾)~",
    // "@('_')@",
    "龴ↀ◡ↀ龴",
    // "༼☉ɷ⊙༽",
    // " (⌒▽⌒)﻿",
    // "ˁ˚ᴥ˚ˀ",
    // "{:{|}",
    // "  ,,    nˁ˚ᴥ˚ˀ",
]

var getAnimal = function() {
    var n = animals.length;
    var i = Math.min( Math.round (Math.random() * n), n-1 )
    return animals[i];
}


var dir = 'r'
var w = window.innerWidth - 80
var h = window.innerHeight - 120
const P = 9

var switchDir = function() {
    dir = (dir === 'r')? 'l':'r'
}
var atEdge = function(p, w) {
    return (dir === 'r')? p >= (w/2) : p <= -200
}

var shuffle = function(strength, delay) {
    setTimeout( function() {
        var animals = document.querySelectorAll('.c');
        var w = window.innerWidth
        animals.forEach( function(animal) {
            if (!animal.style.top)
                animal.style.top = '1px'
            if (!animal.style.left)
                animal.style.left = '1px'


            var left = Math.round(Math.random() * strength)

            var animalPos = parseInt(animal.style.left)

            if (atEdge(animalPos, w))
                switchDir()

            alpha = Math.max(0.3, Math.random())
            r = Math.floor(Math.random() * 255)
            g = Math.floor(Math.random() * 255)
            b = Math.floor(Math.random() * 255)
            padding = Math.floor(Math.random() * P) + 'rem'
            size = padding;

            animal.style.left = (dir === 'r'? animalPos + left : animalPos - left) + 'px'
            // animal.style.backgroundColor = 'rgba('+r+','+g+','+b+',' +alpha+ ')';
            // animal.style.backgroundColor = 'rgba('+r+','+g+','+b+',' +alpha+ ')';
            // animal.style.color = 'rgba('+(r-50)+','+(g-50)+','+(b-50)+',' +alpha+ ')';
            // animal.style.fontSize = size
            // animal.style.padding = padding;
            // animal.style.boxShadow = '0 0 100px rgba('+r+','+g+','+b+',' +alpha+ ')';
            // animal.style.textShadow = '0 0 50px rgba('+r+','+g+','+b+',' +alpha+ ')';
            animal.style.textShadow = '0 0 100px rgb('+r+','+g+','+b+')';
        })

    }, delay)
}
var shuffleV = function(strength, delay) {
    setTimeout( function() {
        var animals = document.querySelectorAll('.c');
        animals.forEach( function(animal) {
            var top = Math.round(Math.random() * strength)
            alpha = Math.max(0.3, Math.random())
            r = Math.floor(Math.random() * 255)
            g = Math.floor(Math.random() * 255)
            b = Math.floor(Math.random() * 255)
            padding = Math.floor(Math.random() * P) + 'rem'
            size = padding;

            animal.style.top = top + 'px'
            animal.style.backgroundColor = 'rgba('+r+','+g+','+b+',' +alpha+ ')';
            animal.style.color = 'rgba('+(r-50)+','+(g-50)+','+(b-50)+',' +alpha+ ')';
            animal.style.fontSize = size
            animal.style.padding = padding;
            animal.style.boxShadow = '0 0 100px rgba('+r+','+g+','+b+',' +alpha+ ')';
            // animal.style.textShadow = '0 0 50px rgba('+r+','+g+','+b+',' +alpha+ ')';
            animal.style.textShadow = '0 0 100px rgb('+r+','+g+','+b+')';
        })
    }, delay)
}

var shuffleZ = function() {
    var chars = document.querySelectorAll('.c');
    var animals = document.querySelectorAll('.a');
    chars.forEach( function(char) {
        var z = Math.floor(Math.random() * chars.length)
        alpha = Math.max(0.3, Math.random())
        padding = Math.floor(Math.random() * P) + 'rem'
        size = padding;

        char.style.zIndex= z;
        char.style.backgroundColor = 'rgba(136,228,255,' +alpha+ ')';
        char.style.color = 'rgba('+(r-50)+','+(g-50)+','+(b-50)+',' +alpha+ ')';
        char.style.padding = padding
        char.style.fontSize = size
        char.style.boxShadow = '0 0 100rgba('+r+','+g+','+b+',' +alpha+ ')';
        char.style.textShadow = '0 0 100px rgb('+r+','+g+','+b+')';
    })
}

var pop = function(char) {
    document.querySelector('body').removeChild(char)
    var audio = new Audio('./audio_file.mp3');
    audio.play();

}

var addChar = function(char) {
    console.log(char.innerText)
    var c = char.innerText
    var r = document.querySelector('.result')
    r.append(c)

}
var delChar = function() {
    var r = document.querySelector('.result')
    var text = r.innerText
    r.innerText = text.substring( 0, text.length - 1)
}

var submit = function() {
    var r = document.querySelector('.result')
    var s = document.querySelector('.s')
    s.innerText = r.innerText
    console.log(r.innerText, s.innerText)
    // s.innerText = 'Sorry, Try again! '
    setTimeout(function() {
        s.innerText = ''
        r.innerText = ''
        shuffleV(h,100)
    }, 1000)
}
var z, alpha, padding, size, r,g,b
const N = animals.length;
for (var i=0; i< N; i++) {

    // z = Math.floor(Math.random() * N)
    alpha = Math.max(0.3, Math.random())
    r = Math.floor(Math.random() * 255)
    g = Math.floor(Math.random() * 255)
    b = Math.floor(Math.random() * 255)
    padding = Math.floor(Math.random() * P) + 'rem'
    size = padding;
    var node = "<div class='c' onmousedown='pop(this);shuffleZ();addChar(this);shuffleV(h, 100)' onmouseup='shuffleV(h, 100)' " +
        "onmouseover='shuffle(45, 310)' style='background-color: rgba("+r+","+g+","+b+"," +alpha+ " );padding: " +padding+ ";z-index:"+z+";font-size:" +size+ "'>" +animals[i]+ "</div>"
    document.write(node)
    shuffle(50, 100)
    shuffleV(h,1)
}

setInterval(function() {shuffleV(h,1)}, 4000)
// setInterval(function() {shuffle(300, 100); shuffleV(300,100)}, 1500)
setInterval(function() {shuffle(45, 1)}, 1000)
/*
    for (var i=0; i< 20; i++) {
        var a = getAnimal()
        var node = "<div class='a' onmousedown='shuffle(30, 100)' onmouseup='shuffle(10, 100)' " +
            "onmouseover='shuffle(5, 10)'>" +a+ "</div>"
        document.write(node)
        shuffle(25, 100)
    }
*/
