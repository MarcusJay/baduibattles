var words = ['Pineapple']
var chars = 'abcdefghijklmnopqrstvqxyzABCDEFGHIJKLMNOPQRSTVQXYZ0123456789'
var animals = [
    "Ƹ̵̡Ӝ̵̨̄Ʒ",
    "(,,,)=(^.^)=(,,,)",
    "~(‾▿‾)~",
    "@('_')@",
    "龴ↀ◡ↀ龴",
    "^⨀ᴥ⨀^",
    "༼☉ɷ⊙༽",
    " (⌒▽⌒)﻿",
    "ˁ˚ᴥ˚ˀ",
    "{:{|}",
    "  ,,    nˁ˚ᴥ˚ˀ",
    "^ↀᴥↀ^",
    "ヽ(￣(ｴ)￣)ﾉ",
    "ʕ•ᴥ•ʔ",
    "ˁ(⦿ᴥ⦿)ˀ"
]

var getAnimal = function() {
    var n = animals.length;
    var i = Math.min( Math.round (Math.random() * n), n-1 )
    return animals[i];
}


var dir = 'r'
var w = window.innerWidth - 80
var h = window.innerHeight - 120

var switchDir = function() {
    dir = (dir === 'r')? 'l':'r'
}
var atEdge = function(p, w) {
    return (dir === 'r')? p >= w : p <= 0
}

var shuffle = function(strength, delay) {
    setTimeout( function() {
        var animals = document.querySelectorAll('.c');
        var w = window.innerWidth
        animals.forEach( function(animal) {
            if (!animal.style.top)
                animal.style.top = '1px'
            if (!animal.style.left)
                animal.style.left = '1px'


            var top = Math.round(Math.random() * strength)
            var left = Math.round(Math.random() * strength)

            // animal.style.top = (parseInt(animal.style.top) + top) + 'px'
            var animalPos = parseInt(animal.style.left)
            // console.log(animalPos, w)
            if (atEdge(animalPos, w))
                switchDir()

            animal.style.left = (dir === 'r'? animalPos + left : animalPos - left) + 'px'            })
    }, delay)
}
var shuffleV = function(strength, delay) {
    setTimeout( function() {
        var animals = document.querySelectorAll('.c');
        animals.forEach( function(animal) {
            var top = Math.round(Math.random() * strength)

            animal.style.top = top + 'px'
        })
    }, delay)
}

var shuffleZ = function() {
    var chars = document.querySelectorAll('.c');
    chars.forEach( function(animal) {
        var z = Math.floor(Math.random() * chars.length)
        alpha = (z+0.25) / chars.length //  Math.max(0.77, Math.random())
        padding = ((z*3) / chars.length) + 'rem' //Math.floor(Math.random() * 5) + 'rem'
        size = padding;

        animal.style.zIndex= z;
        animal.style.backgroundColor = 'rgba(136,228,255,' +alpha+ ')';
        animal.style.padding = padding
        animal.style.fontSize = size
    })
}

var addChar = function(char) {
    console.log(char.innerText)
    var c = char.innerText
    var r = document.querySelector('.result')
    r.append(c)

}
var delChar = function() {
    var r = document.querySelector('.result')
    var text = r.innerText
    r.innerText = text.substring( 0, text.length - 1)
}

var submit = function() {
    var r = document.querySelector('.result')
    var s = document.querySelector('.s')
    s.innerText = r.innerText
    console.log(r.innerText, s.innerText)
    // s.innerText = 'Sorry, Try again! '
    setTimeout(function() {
        s.innerText = ''
        r.innerText = ''
        shuffleV(h,100)
    }, 1000)
}
var z, alpha, padding, size
for (var i=0; i< chars.length; i++) {
    // var a = getAnimal()

    z = Math.floor(Math.random() * chars.length)
    alpha = (z+0.25) / chars.length //  Math.max(0.77, Math.random())
    padding = ((z*3) / chars.length) + 'rem' //Math.floor(Math.random() * 5) + 'rem'
    size = padding;
    var node = "<div class='c' onmousedown='shuffleZ();addChar(this);shuffleV(h, 100)' onmouseup='shuffleV(h, 100)' " +
        "onmouseover='shuffle(45, 310)' style='background-color: rgba(136,228,255," +alpha+ " );padding: " +padding+ ";z-index:"+z+";font-size:" +size+ "'>" +chars[i]+ "</div>"
    document.write(node)
    shuffle(250, 100)
    shuffleV(h,1)
}

setInterval(function() {shuffleV(h,1)}, 4000)
// setInterval(function() {shuffle(300, 100); shuffleV(300,100)}, 1500)
setInterval(function() {shuffle(45, 1)}, 1000)
/*
    for (var i=0; i< 20; i++) {
        var a = getAnimal()
        var node = "<div class='a' onmousedown='shuffle(30, 100)' onmouseup='shuffle(10, 100)' " +
            "onmouseover='shuffle(5, 10)'>" +a+ "</div>"
        document.write(node)
        shuffle(25, 100)
    }
*/
